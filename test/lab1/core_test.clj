(ns lab1.core-test
  (:require [clojure.test :refer :all]
            [lab1.task_13 :refer :all]
            [lab1.task_18 :refer :all]))

(deftest task-13-test
  (testing "task-13"
    (is (= (report-013) 5537376230))
    (is (= (report-013-loop) 5537376230))
    (is (= (report-013-rec) 5537376230)))) 

(deftest task-18-test
  (testing "task-18"
    (is (= (report-018) 1074))
    (is (= (report-018-loop) 1074))
    (is (= (report-018-recur) 1074))))