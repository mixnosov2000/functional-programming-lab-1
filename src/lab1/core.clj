(ns task1.core
  (:require [task1.task_13 :refer [task-13-report]]
            [task1.task_18 :refer [task-18-report]]))

(defn -main []
  (task-13-report)
  (task-18-report))
