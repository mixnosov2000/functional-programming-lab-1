(ns task1.task_18)

(def triangle
  [[75]
   [95 64]
   [17 47 82]
   [18 35 87 10]
   [20 04 82 47 65]
   [19 01 23 75 03 34]
   [88 02 77 73 07 63 67]
   [99 65 04 28 06 16 70 92]
   [41 41 26 56 83 40 80 70 33]
   [41 48 72 33 47 32 37 16 94 29]
   [53 71 44 65 25 43 91 52 97 51 14]
   [70 11 33 28 77 73 17 78 39 68 17 57]
   [91 71 52 38 17 14 91 43 58 50 27 29 48]
   [63 66 04 68 89 53 67 30 73 16 69 87 40 31]
   [4 62 98 27 23 9 70 98 73 93 38 53 60 4 23]])

(defn pair-row
  [row]
  (partition 2 1 row))

(defn max-pair-row
  [paired-row]
  (map #(reduce max %) paired-row))

(defn reduce-rows
  [a b]
  (map + (max-pair-row (pair-row a)) b))

(defn report-018
  ([] (report-018 triangle))
  ([t] (first (reduce reduce-rows (reverse t)))))

(defn triangle-loop [t]
  (loop [acc (first t) rest-t (rest t)]
    (if (empty? rest-t)
      acc
      (recur (reduce-rows acc (first rest-t)) (rest rest-t)))))
    
(defn report-018-loop
  ([] (report-018-loop triangle))
  ([t] (first (triangle-loop (reverse t)))))

(defn triangle-recur [t]
  (if (= (count t) 1)
    (first t)
    (reduce-rows (triangle-recur (rest t)) (first t))))

(defn report-018-recur
  ([] (report-018-recur triangle))
  ([t] (first (triangle-recur t))))

(defn task-18-report []
  (do
   (println "Task 18 solutions:
              * report-018
              * report-018-loop
              * report-018-recur")
   (println "Results are equal:"
            (apply = [(time (report-018))
                      (time (report-018-loop))
                      (time (report-018-recur))]))
   (println "Solution:" (report-018))))