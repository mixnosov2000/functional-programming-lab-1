# Лабораторная работа #1

**Дисциплина:** "Функциональное программирование"

**Выполнил:** Носов Михаил, P34102

**Название:** "Решение задач Project Euler"

**Цель работы:** 
Решить две задачи project euler, различными способами используя выбранный ранее функциональный ЯП.

**Требования**
1. монолитные реализации с использованием:
   - хвостовой рекурсии;
   - рекурсии (вариант с хвостовой рекурсией не является примером рекурсии);
2. модульной реализации, где явно разделена генерация последовательности, фильтрация и свёртка (должны использоваться функции reduce/fold, filter и аналогичные);
3. генерация последовательности при помощи отображения (map);
4. работа со спец. синтаксисом для циклов (где применимо);
5. работа с бесконечными списками для языков поддерживающих ленивые коллекции или итераторы как часть языка (к примеру Haskell, Clojure);
6. реализация на любом удобном для вас традиционном языке программировании для сравнения.

### Условия задач

**1. Задача 13:**
```
    Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
```
  
**2. Задача 18:**
```
    By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

	   3
	  7 4
	 2 4 6
	8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below.
```

#### Окружение и плагины
```
    (defproject lab1 "0.1.0-SNAPSHOT"
  :description "lab1 for functional programming"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
		 [org.clojure/math.numeric-tower "0.0.4"]]
  :main lab1.core
  :aot [lab1.core]
  :target-path "target/%s"
  :plugins [[lein-cljfmt "0.9.0"]]
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})

```


#### Выполнения задания №13:

"В лоб":
```
(defn get-first-ten
  [number]
  (read-string (.substring (str number) 0 10)))

(defn report-013
  ([] (report-013 t))
  ([t] (get-first-ten (apply + t))))

```
Хвостовой рекурсией:
```
(defn get-digit
  [i num]
  (read-string (str (.charAt (str num) i))))

(defn sum-loop [numbers]
  (get-first-ten (loop [index 49
                       num numbers
                       c 0]
                  (if (> index 0)
                    (if (> index 9)
                      (let [carry (+ c (reduce + (map #(get-digit index %1) numbers)))]
                        (recur (- index 1) num (quot carry 10)))
                      (let [carry (+ c (* (reduce + (map #(get-digit index %1) numbers)) (expt 10 (- 11 index))))]
                        (recur (- index 1) num carry)))
                    (+ c (* (reduce + (map #(get-digit index %1) numbers)) (expt 10 (- 10 index))))))))

(defn report-013-loop
  ([] (report-013-loop t))
  ([t] (sum-loop t)))

```
Рекурсией:
```
(defn sum-recur 
  ([numbers] (sum-recur numbers 0 0))
  ([numbers index bod] 
   (if (and (or (> (+ (* 10 bod) (+ (reduce + (map #(get-digit index %1) numbers)) 99)) 1000) (< index 11)) (< index 49))
     (+ (sum-recur numbers (+ index 1) (mod (reduce + (map #(get-digit index %1) numbers)) 100)) (* (reduce + (map #(get-digit index %1) numbers)) (expt 10 (- 16 index))))
     (* (reduce + (map #(get-digit index %1) numbers)) (expt 10 (- 9 index)))
     ))
  )

(defn recur-call [numbers]
  (get-first-ten (quot (sum-recur numbers) 1)))

(defn report-013-rec
  ([] (report-013-rec t))
  ([t] (recur-call t)))

```
На java
```
public String sumBigInt(String[] numbers) {
	BigInteger sum = BigInteger.ZERO;
	for (String num : numbers)
		sum = sum.add(new BigInteger(num));
	return sum.toString().substring(0, 10);
}

```
#### Выполнения задания №18:
Модульное:
```
(defn pair-row
  [row]
  (partition 2 1 row))

(defn max-pair-row
  [paired-row]
  (map #(reduce max %) paired-row))

(defn reduce-rows
  [a b]
  (map + (max-pair-row (pair-row a)) b))

(defn report-018
  ([] (report-018 triangle))
  ([t] (first (reduce reduce-rows (reverse t)))))
```
Хвостовая рекурсия:
```
(defn triangle-loop [t]
  (loop [acc (first t) rest-t (rest t)]
    (if (empty? rest-t)
      acc
      (recur (reduce-rows acc (first rest-t)) (rest rest-t)))))
    
(defn report-018-loop
  ([] (report-018-loop triangle))
  ([t] (first (triangle-loop (reverse t)))))
```
Рекурсия:
```
(defn triangle-recur [t]
  (if (= (count t) 1)
    (first t)
    (reduce-rows (triangle-recur (rest t)) (first t))))

(defn report-018-recur
  ([] (report-018-recur triangle))
  ([t] (first (triangle-recur t))))
```
На java
```
public String trianglePath(int[][] triangle) {
    for (int i = triangle.length - 2; i >= 0; i--) {
        for (int j = 0; j < triangle[i].length; j++)
        triangle[i][j] += Math.max(triangle[i + 1][j], triangle[i + 1][j + 1]);  // Dynamic programming
    }
    return Integer.toString(triangle[0][0]);
}
```
Результаты выполнения
```
Task 13 solutions:
              * report-013
              * report-013-loop
              * report-013-rec
"Elapsed time: 0.5183 msecs"
"Elapsed time: 40.8373 msecs"
"Elapsed time: 19.3506 msecs"
Results are equal: false
Solution: 5537376230
Task 18 solutions:
              * report-018
              * report-018-loop
              * report-018-recur
"Elapsed time: 0.8373 msecs"
"Elapsed time: 2.4433 msecs"
"Elapsed time: 0.7078 msecs"
Results are equal: true
Solution: 1074
```
результаты java 
```
task6Result: 5537376230
Time used to calculate in milliseconds: 4.154
task25Result: 1074
Time used to calculate in milliseconds: 0.895
```

**Заключение**

Было тяжело не пользоваться переменными и преобразование между структурами данных шло иногда очень плохо. Плюс, несмотря на подсказки ide, легко запутаться в скобках и порядке функций и передаваемых им значений.